import 'package:flutter/material.dart';
import 'package:minggu_4/main.dart';

import 'Tugas12/Telegram.dart';
import 'Tugas12/BelajarImage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: {
        //'/': (context) => Telegram(),
        '/': (context) => BelajarImage(),
      },
    );
  }
}