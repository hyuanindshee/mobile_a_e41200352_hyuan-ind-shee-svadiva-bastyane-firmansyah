import 'package:flutter/material.dart';
import 'package:flutter_navigation/routes.dart';

void main() {
  runApp(MaterialApp(
    onGenerateRoute: RouteGenerator.generateRoute,
  ));
}
